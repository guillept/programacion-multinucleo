/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdasYStreams;

import java.math.BigInteger;
import java.util.stream.IntStream;

/**
 *
 * @author guill
 */
public class StreamFactorial {
    
    public static void main(String[] args) {
        final int n = 250_000;
        
        long inicio = System.nanoTime();
        //BigInteger result = IntStream.rangeClosed(1, n).mapToObj(BigInteger::valueOf).reduce(BigInteger.ONE, BigInteger::multiply); //T1 = 19.6806
        BigInteger result = IntStream.rangeClosed(1, n).parallel().mapToObj(BigInteger::valueOf).reduce(BigInteger.ONE, BigInteger::multiply); //T1 = 1.4318
        long fin = System.nanoTime();
       System.out.printf("Result = %d  T1 = %.4f%n", result, (fin - inicio)/1E9);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdasYStreams;

import java.util.stream.IntStream;

/**
 *
 * @author guill
 */
public class StreamPi {
    
    private static final int NUM_RECTS = 5_000_000_00;
    private static final double WIDTH = 1.0 / NUM_RECTS;
    
    private static double doit(int i){
       double mid = (i + 0.5) * WIDTH;
       return 4.0 / (1.0 + mid * mid);
    }    
    public static void main(String[] args) {
        
        long inicio = System.nanoTime();
        double sum = IntStream.range(0, NUM_RECTS).mapToDouble(StreamPi::doit).sum();
        double area = sum * WIDTH;
        long fin = System.nanoTime();
        System.out.printf("Resultado: %f, Tiempo = %.2f%n", area, (fin-inicio)/1E9);
        // Resultado: 3.141593, Tiempo = 4.50
        
        inicio = System.nanoTime();
        sum = IntStream.range(0, NUM_RECTS).parallel().mapToDouble(StreamPi::doit).sum();
        area = sum * WIDTH;
        fin = System.nanoTime();
        System.out.printf("Resultado: %f, Tiempo(n) = %.2f%n", area, (fin-inicio)/1E9);
        // Resultado: 3.141593, Tiempo(n) = 1.97
    }
    
}

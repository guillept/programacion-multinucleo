/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdasYStreams;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 *
 * @author guill
 */
public class EjemplosStreams {
    
    public static void main(String[] args) {
        IntStream insstream = IntStream.rangeClosed(1, 1000).map(x -> x*x);
        
        /* 
        IntStream solo se puede usar una vez para dar un resultado final
        int[] array =  insstream.toArray();
        System.out.println(Arrays.toString(array)); */
        
        /* int suma = insstream.reduce(0, (a,b) -> a + b);
        System.out.println(suma); */
        
        Stream<String> strings = insstream.mapToObj(x -> Integer.toString(x));
        strings.forEach(System.out::println);
    }
    
}

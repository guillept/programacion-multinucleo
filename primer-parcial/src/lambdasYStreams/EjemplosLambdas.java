/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lambdasYStreams;

import java.util.function.Predicate;

/**
 *
 * @author guill
 */

interface MiInterfaz {
   String justDoIt(boolean b, int x);
}

public class EjemplosLambdas {

            
    
    public static void main(String[] args) {
        Runnable corredor = new Runnable() {
            @Override
            public void run() {
                System.out.println("Hola");
            }
        };
        corredor.run();
        
        Runnable corredor2 = () -> {  System.out.println("Aios"); };
        corredor2.run();
        
        Predicate<String> p =  s -> s.length() > 10;
        System.out.println(p.test("Estoy con el novio de Marge"));
        
        MiInterfaz mi = (bo, in) -> "Hola Mundo!";
        System.out.println(mi.justDoIt(true, 0));
    }
    
}

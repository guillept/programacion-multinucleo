/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorialConThreads;

import java.util.LinkedList;
import java.util.Queue;

/**
 *
 * @author guille
 */

class Flipper {
    
    private final Queue<Corredor> fila = new LinkedList<>(); 
    
    public synchronized void add(Corredor corredor) {
        fila.offer(corredor);
    }
    
    public synchronized void flip() {
        fila.offer(fila.poll());
    }
    
   public synchronized Corredor turnoActual() {
       return fila.peek();
   }
}
public class Corredor implements Runnable{
    
    private final Flipper flipper;
    
    public Corredor(Flipper flipper){
        this.flipper = flipper;
        flipper.add(this);
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        Flipper flipper = new Flipper();
        Corredor c1 = new Corredor(flipper);
        Corredor c2 = new Corredor(flipper);
        Thread t1 = new Thread(c1, "Thread(0)");
        Thread t2= new Thread(c2,  "Thread(1)");
        
        t1.start();
        t2.start();
        
        t1.join();
        t2.join();
    }

    @Override
    public void run() {
        // for (int i = 0; i < 100; i++) { //intercalado
            synchronized (flipper) {
                while (flipper.turnoActual() != this) { //not condicion para continuar con X
                    try {
                        flipper.wait();  //release lock and wait for your turn
                    }catch (InterruptedException e) {
                        e.printStackTrace();                      
                    } 
                }
                for (int i = 0; i < 100; i++) {
                    System.out.printf("%s: %2d%n", Thread.currentThread().getName(), i);
                }
                flipper.flip();
                flipper.notifyAll();
            }
        // }
    }
    
}

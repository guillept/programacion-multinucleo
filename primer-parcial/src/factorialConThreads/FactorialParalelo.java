/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factorialConThreads;

import java.math.BigInteger;

/**
 *
 * @author guill
 */
public class FactorialParalelo implements Runnable {
    
    private int inicio;
    private int fin;
    private BigInteger acumulado;

    public FactorialParalelo(int inicio, int fin) {
        this.inicio = inicio;
        this.fin = fin;
    }
    
    @Override
    public void run() {
        acumulado = BigInteger.ONE;
        for (int i = inicio; i <= fin; i++) {
            acumulado = acumulado.multiply(BigInteger.valueOf(i));
        }
    }
    
    public static BigInteger factorial(int n) {
        
        // Factorial Paralelo = Runnable
        FactorialParalelo f1 = new FactorialParalelo(1, n / 2);
        FactorialParalelo f2 = new FactorialParalelo(n / 2 + 1, n);
        
        Thread t1 = new Thread(f1);
        Thread t2 = new Thread(f2);
        
        t1.start();
        t2.start();
        
        /* Para usar un solo thread se llama a f2.run() sin crear 2 threads */
        
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e){
            //Nunca pasa
        
        }
        
        return f1.acumulado.multiply(f2.acumulado);
    }
    
    public static void main(String[] args) {
        /**
         * Resultado Secuencial
         * T1 = 2.3881
         * r = 1105792
         * S = T1 / T2 = 6.4947 / 2.3881 = 2.71
         */
        long inicio = System.nanoTime();
        BigInteger r = factorial(150_000);
        long fin = System.nanoTime();
        System.out.printf("T1 = %.4f%n", (fin - inicio)/1E9);
        System.out.println(r.bitCount());
    }
    
}

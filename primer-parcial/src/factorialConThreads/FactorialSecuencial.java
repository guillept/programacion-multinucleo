package factorialConThreads;

import java.math.BigInteger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author guill
 */
public class FactorialSecuencial {
    
    public static BigInteger factorial(int n) {
        BigInteger resultado = BigInteger.ONE;
        for (int i = 1; i <= n; i++) {
            resultado = resultado.multiply(BigInteger.valueOf(i));
        }
        return resultado;
    }
    
    public static void main(String[] args) {
        /**
         * Resultado Secuencial
         * T1 = 6.4947
         * r = 1105792
         */
        long inicio = System.nanoTime();
        BigInteger r = factorial(150_000);
        long fin = System.nanoTime();
        System.out.printf("T1 = %.4f%n", (fin - inicio)/1E9);
        System.out.println(r.bitCount());
        
    }
    
}

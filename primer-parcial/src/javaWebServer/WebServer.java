/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * @author Ariel Ortiz
*/


package javaWebServer;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Starts the web server and listens to client connections. Individual
 * connections are processed by Worker instances.
 */
public class WebServer {

    public static final int PORT_NUMBER = 12345;

    private void listen() {
        System.out.printf(
                "Minuscule web server ready. Listening at port %d...%n",
                PORT_NUMBER);

        try (ServerSocket servSock = new ServerSocket(PORT_NUMBER)) {

            Executor executor = Executors.newFixedThreadPool(5);
            while (true) {
                Socket sock = servSock.accept();
                Worker w = new Worker(sock);
                
                /* new Thread(new Runnable() {
                    public  void run() {
                        w.doWork();
                    }
                }).start(); */
                 
                //new Thread(() -> w.doWork()).start();
                
                executor.execute(() -> w.doWork()); //Multiles threads
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new WebServer().listen();
    }
}
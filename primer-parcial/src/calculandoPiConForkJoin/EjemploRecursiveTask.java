/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculandoPiConForkJoin;

import static calculandoPiConForkJoin.EjemploRecursiveTask.PiTask.parallelPi;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 *
 * @author guill
 */
public class EjemploRecursiveTask {

    private static final long NUM_RECTS = 100_000_000;
    
    public static class PiTask extends RecursiveTask<Double> {

        private static final long serialVersionUID = 1L; //permite revisar si son errores cmpatibles tras la deserelizacion
        
        public static final int UMBRAL = 1_000;
        double width;
        private long lo, hi;
        public PiTask(long lo, long hi, double width){
            this.lo = lo;
            this.hi = hi;
            this.width = width;
        }
        
        @Override
        protected Double compute() {
            if (hi - lo < UMBRAL) {
                double mid, height, sum = 0.0;

                for (long i = lo; i <= hi; i++) {
                    mid = (i + 0.5) * width;
                    height = 4.0 / (1.0 + mid * mid);
                    sum += height;
                }
                return width * sum;
            } else {
                long mid = (hi + lo) >>> 1;
                PiTask t2 = new PiTask(mid + 1, hi, width);
                PiTask t1 = new PiTask(lo, mid, width);
                t1.fork(); //paralelo
                
                double r2 = t2.compute();
                double r1 = t1.join();
                
                return r1 + r2;
            }                 
        }
        
        public static double parallelPi() {
            ForkJoinPool pool = new ForkJoinPool();
            PiTask t = new PiTask(0, NUM_RECTS - 1, 1.0/ NUM_RECTS);
            return pool.invoke(t);
        }
    }
    
    
    public static double  sequentialPi () {
        double mid, height, width, area;
        double sum = 0.0;

        width = 1.0 / (double) NUM_RECTS;
        for (long i = 0; i < NUM_RECTS; i++) {
            mid = (i + 0.5) * width;
            height = 4.0 / (1.0 + mid * mid);
            sum += height;
        }
        area = width * sum;
        return area;
        
    }
    
    public static void main(String[] args) {
        long inicio = System.nanoTime();
        double result = sequentialPi();
        long fin = System.nanoTime();
        double time = (fin - inicio)/1E9;
        //Pi = 3.1415926536 T1 = 1.2357
        System.out.printf("Pi = %.10f T1 = %.4f%n", result, time );
        
        inicio = System.nanoTime();
        result = parallelPi();
        fin = System.nanoTime();
        double time8 = (fin - inicio)/1E9;
        System.out.printf("Pi = %.10f T8 = %.4f%n", result, time8 );
        
        double speedup = time / time8;
        System.out.printf("Speeddup = %.4f%n", speedup );
        
        /*
        Pi = 3.1415926536 T1 = 1.1733
        Pi = 3.1415926536 T8 = 0.3295
        Speeddup = 3.5612
        */
        
    }
    
}

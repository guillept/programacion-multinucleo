/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculandoPiConForkJoin;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 *
 * @author guill
 */
public class EjemploRecursiveAction {
    
    public static class SquareAction extends RecursiveAction {
        
        private int hi, lo;
        private int[]array;
        public static final int UMBRAL = 1_000;
        
        public SquareAction(int lo, int hi, int[] array) {
            this.hi = hi;
            this.lo = lo;
            this.array =  array;
        }

     
        @Override
        protected void compute() {
            if (hi - lo < UMBRAL) {
                for (int i = lo; i <= hi; i++) {
                   int suma = 0;
                    for (int j = 0; j < array[i]; j++) {
                       suma += array[i];
                        for (int k = 0; k < 100000; k++);
                    }
                    array[i] = suma;
                }
            } else {
                int mid = (hi + lo) >>> 1;
                SquareAction ar = new SquareAction(lo, mid, array);
                SquareAction a2 = new SquareAction(mid + 1, hi, array);
                invokeAll(ar, a2);
            }
        }
    }
        
    private static void parallelSquare(int[] array){
        ForkJoinPool pool = new ForkJoinPool();
        SquareAction a = new SquareAction(0, array.length -1, array);
        pool.invoke(a);

    }

    private static final int ARRAY_SIZE = 1_000_000;

    public static void main(String[] args) {
        int[] array = new int[ARRAY_SIZE];
        Arrays.fill(array, 42);
        parallelSquare(array);
        System.out.println("Epa epa mi piernita");

    }
        
    
}

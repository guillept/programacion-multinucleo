/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//arraycopy(Object src, int srcPos, Object dest, int destPos, int length)

package proyectoFinal;

import java.util.Arrays;
import java.util.Random;

public class Main {
    
    private static final int SIZE = 10_000_000;
    private static long sequentialTime = 0;
    public static void main(String[] args) {
        
        Integer[] originalArray = new Integer[SIZE];
        for (int i = 0; i < originalArray.length; i++)
           originalArray[i] =  new Random().nextInt(100);    

        Integer[] copy = new Integer[SIZE];
        System.arraycopy(originalArray, 0, copy, 0, originalArray.length);

        /*Sequential Sort*/
        long inicio = System.nanoTime();
        Integer[] sortedSequential = sequentialSort(copy);
        long fin = System.nanoTime();
        sequentialTime = fin - inicio;
        System.out.printf("Tiempo Secuencial = %.4f%n", sequentialTime/1E9);
        System.out.println("* * * * * * * * *\n");

        /*Fork/Join en Java*/
        mergeSort(originalArray, sortedSequential, "Fork Join en Java");

        /*Threads*/
        mergeSort(originalArray, sortedSequential, "Threads en Java");
        
        /*Streams*/
        mergeSort(originalArray, sortedSequential, "Streams paralelos en Java");
    }
    
    private static Integer[] sequentialSort(Integer[] array) {
        System.out.println("* * * * * * * * *");
        System.out.println("Merge Sort Secuencial en Java");
        return MergeSort.mergeSort(array); 
    }

    private static void mergeSort(Integer[] originalArray, Integer[] sortedSequential, String algoritmo) {   
        System.out.println("* * * * * * * * *");
        
        Integer[] usortedParallel = new Integer[SIZE];
        System.arraycopy(originalArray, 0, usortedParallel, 0, originalArray.length);
       
        Integer[] parallelArray = new Integer[SIZE];
        long inicio = System.nanoTime();
        
        switch(algoritmo){
            case "Fork Join en Java":
                parallelArray = MergeSortParallel.merge(usortedParallel);
                break;
            case "Threads en Java":
                parallelArray = MergeSortThreads.merge(usortedParallel);
                break;
            case "Streams paralelos en Java":
                parallelArray = MergeSortStreamParallel.merge(usortedParallel);
                break;
        }
        
        long fin = System.nanoTime();
        long parallelTime = fin - inicio;
        System.out.printf("Tiempo usando %s = %.4f%n", algoritmo, parallelTime/1E9);
        double speedup = (sequentialTime/1E9)/(parallelTime/1E9);
        System.out.printf("Speedup = %.4f%n", speedup);
        
        compareArrays(sortedSequential, parallelArray, algoritmo);
    }    
   
    private static void compareArrays(Integer[] originalArray, Integer[] parallelArray, String algoritmo){
        System.out.printf("El arreglo original y el arreglo utilizando %s %s son iguales\n", 
            algoritmo, (Arrays.equals(originalArray, parallelArray) ? "si": "no"));
        System.out.println("* * * * * * * * *\n");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoFinal;

import java.util.Arrays;

public class MergeSortStreamParallel {

    private static Integer[] result, sortedArray;
    private static int lo = 0;

    private static void mapper(Integer[] array) {
        sortedArray = MergeSort.mergeSort(array);
        System.arraycopy(sortedArray, 0, result, lo, sortedArray.length);
        lo = array.length;
    }
    
    public static Integer[] parallelCountSort(Integer[] arr) {
        
        Integer[] leftArray = new Integer[arr.length/2];
        Integer[] rightArray = new Integer[arr.length - leftArray.length];
        System.arraycopy(arr, 0, leftArray, 0, leftArray.length);
        System.arraycopy(arr, leftArray.length, rightArray, 0, rightArray.length);
        
        Integer[][] arrays =  new Integer[2][];
        arrays[0] = leftArray;
        arrays[1] = rightArray;
        
        Arrays.stream(arrays).parallel().forEach(MergeSortStreamParallel::mapper);
        return MergeSort.mergeSort(result); //final sort
    }
    
    public static Integer[] merge(Integer[] usortedParallel) {
        result = new Integer[usortedParallel.length];
        return parallelCountSort(usortedParallel);
    }
}

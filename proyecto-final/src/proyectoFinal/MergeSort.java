/*
Algorithm taken from: https://es.wikipedia.org/wiki/Ordenamiento_por_mezcla
*/

package proyectoFinal;

public class MergeSort {
    
    public static Integer[] mergeSort(Integer[] arr) {
        if(arr.length <= 1) {
            return arr;
        }
        
        Integer[] leftArray = new Integer[arr.length/2];
        Integer[] rightSort = new Integer[arr.length - leftArray.length];
        //arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
        System.arraycopy(arr, 0, leftArray, 0, leftArray.length);
        System.arraycopy(arr, leftArray.length, rightSort, 0, rightSort.length);
        mergeSort(leftArray);
        mergeSort(rightSort);
        merge(leftArray, rightSort, arr);
        return arr;
    }

    public static void merge(Integer[] leftArray, Integer[] rightArray, Integer[] resultList) {
        int leftIndex = 0, rightIndex = 0, i = 0;

        while(leftIndex < leftArray.length && rightIndex < rightArray.length) {
            if(leftArray[leftIndex].compareTo(rightArray[rightIndex]) < 0) {
                resultList[i] = leftArray[leftIndex];
                leftIndex++;
            }else {
                resultList[i] = rightArray[rightIndex];
                rightIndex++;
            }
            i++;
        }
        // arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
        System.arraycopy(leftArray, leftIndex, resultList, i, leftArray.length - leftIndex);
        System.arraycopy(rightArray, rightIndex, resultList, i, rightArray.length - rightIndex);
    }
}
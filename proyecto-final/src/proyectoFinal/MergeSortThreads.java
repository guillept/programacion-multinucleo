/*
Algorithm taken from: https://es.wikipedia.org/wiki/Ordenamiento_por_mezcla
*/

package proyectoFinal;

public class MergeSortThreads implements Runnable {
    
    private Integer[] res;
    private final Integer[] arr;

    public MergeSortThreads(Integer[] arr) {
        this.arr = arr;
    }
    
    @Override
    public void run() {
        res =  mergeSort(arr);
    }
    
    public Integer[] mergeSort(Integer[] array) {
        if(array.length <= 1) {
            return array;
        }
        int size = array.length;
        
        Integer[] leftArray = new Integer[size/2];
        Integer[] rightSort = new Integer[size - leftArray.length];
        System.arraycopy(array, 0, leftArray, 0, leftArray.length);
        System.arraycopy(array, leftArray.length, rightSort, 0, rightSort.length);
        
        mergeSort(leftArray);
        mergeSort(rightSort);
        MergeSort.merge(leftArray, rightSort, array);
        return array;
    }
      
    public static Integer[] merge(Integer[] arr) {
        
        // Split into two arrays      
        Integer[] leftArray = new Integer[arr.length/2];
        Integer[] rightArray = new Integer[arr.length - leftArray.length];
        System.arraycopy(arr, 0, leftArray, 0, leftArray.length);
        System.arraycopy(arr, leftArray.length, rightArray, 0, rightArray.length);

        MergeSortThreads f1 = new MergeSortThreads(leftArray); //sort left array
        MergeSortThreads f2 = new MergeSortThreads(rightArray); //sort rigth array
        
        Thread t1 = new Thread(f1);
        Thread t2 = new Thread(f2);
        
        t1.start();
        t2.start();
     
        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e){
            System.out.println(e);    
        }

        MergeSort.merge(f1.res, f2.res, arr);        
        return arr;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoFinal;

import java.util.concurrent.ForkJoinPool;
import static java.util.concurrent.ForkJoinTask.invokeAll;
import java.util.concurrent.RecursiveAction;

public class MergeSortParallel {
    
    public static class SortTask extends RecursiveAction {

        private static final long serialVersionUID = 1L;

        private final int threads, lo;
        private final Integer[] temp, original;
        private static Integer[] res;
        
        public SortTask(Integer[] original, Integer[] temp, int threads, int low){
            this.original = original;
            this.temp = temp;
            this.threads = threads;
            this.lo = low;
        }

        @Override
        protected void compute() {

            if (threads == 2) {
                res = MergeSort.mergeSort(temp);
                System.arraycopy(res, 0, original, lo, res.length);
            } else {
                int size = original.length;

                Integer[] leftArray = new Integer[size/2];
                Integer[] rightArray = new Integer[size - leftArray.length];
                System.arraycopy(original, 0, leftArray, 0, leftArray.length);
                System.arraycopy(original, leftArray.length, rightArray, 0, rightArray.length);
                
                SortTask ar = new SortTask(original, leftArray, 2, 0);
                SortTask a2 = new SortTask(original, rightArray, 2, leftArray.length);                
                invokeAll(ar, a2);
            }
        }
    }
    
    public static void parallelCountSort(Integer[] arr) {
        ForkJoinPool pool = new ForkJoinPool();
        SortTask t = new SortTask(arr, arr, 1, 0);
        pool.invoke(t);
    }
    
    public static Integer[] merge(Integer[] array) {
        parallelCountSort(array);
        return MergeSort.mergeSort(array); //final sort
    }
}

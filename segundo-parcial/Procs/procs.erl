-module(procs).
-export([factorial/1, fibo_proc/0, fibo_send/2, ring/2]). %ring/2

factorial(N) ->
    P = spawn(fun fact_aux/0),
    P ! {fact, N, self()},
    receive
        Result -> Result
    end.

fact_aux() ->
    receive
        {fact, N, Remitente} -> Remitente ! fact(N)
    end.

fact(0) -> 1;
fact(N) when N > 0 ->
    N * fact(N - 1).


fibo_proc() ->
    spawn(fun () -> fibo_loop([1, 0]) end).

fibo_send(Proc, Mssg) ->
    case is_process_alive(Proc) of
        true ->
            Proc ! {Mssg, self()},
            receive
                X -> X
            end;
        false ->
            killed
    end.

fibo_loop(Nums) ->
    [X, Y | T] = Nums,
    receive
        {recent, Remitente} ->
            Remitente ! X,
            fibo_loop(Nums);
        {span, Remitente} ->
            Remitente ! length(Nums),
            fibo_loop(Nums);
        {_, Remitente} ->
            Remitente ! killed
    after 1000 ->
        fibo_loop([X + Y, X, Y | T])
    end.

% ring starts N processes, and sends a message M times around all the processes in the ring.
% After all the messages have been sent the processes should terminate gracefully.%

ring(N, M) ->
  Manager = self(),
  io:format("Current process is ~w~n",[Manager]),
  spawn(fun() -> start(N,M,Manager) end),
  ok.

start(N,M,Manager) ->
  Pid = spawn(fun() -> start(N-1,M,self(),Manager) end),
  io:format("Created ~w~n",[Pid]),
  ring_aux(1, M, Pid, Manager).

start(0, M, Proc,Manager) ->
  Proc ! start,
  ring_aux(1, M, Proc,Manager);

start(N, M, Proc,Manager) ->
  Pid = spawn(fun() -> start(N-1, M, Proc,Manager) end),
  io:format("Created ~w~n",[Pid]),
  ring_aux(1, M, Pid,Manager).

ring_aux(Count, M, _, _) when Count > M -> 
    io:format("~w: finished~n",[self()]);

ring_aux(Count, M, Remitente,Manager) ->
    receive
        start ->
            io:format("~w: Received ~w/~w from ~w~n", [self(), Count, M, Manager]),
            Remitente ! {start, self()},
            ring_aux(Count+1, M, Remitente, Manager);
        {start, Pid} ->
            io:format("~w: Received ~w/~w from ~w~n", [self(), Count, M, Pid]),
            Remitente ! {start, self()},
            ring_aux(Count+1, M, Remitente, Manager)
    end.

-module(criba).
-export([criba/1]).
-import(plists, [mapreduce/2]). 

criba(E) -> get_criba(lists:seq(2, E)).

get_criba([]) -> [];
get_criba([H|T]) -> [H|get_criba(lists:filter(fun(N) -> N rem H /= 0 end, T))].
%----------------------------------------------------------
% Práctica #5: Erlang secuencial
% Date: March 17, 2019.
% Authors:
%          A01370880 Rubén Escalante Chan
%          A01377162 Guillermo Pérez Trueba
%----------------------------------------------------------

-module(sequential).
-export([but_last/1, insert/2, binary/1, prime_factors/1, encode/1]).

% 
% 1. The function but_last returns a list with the same elements as its input list but excluding the last element. 
% Assume that the input list contains at least one element.
%

but_last([_]) -> [];
but_last([H | T]) -> [H | but_last(T)].

%
% 3. The function insert takes two arguments: a number N and a list of numbers L in ascending order. 
% It returns a new list with the same elements as L but inserting N in its corresponding place.
%

insert(N, []) -> [N];
insert(N, [H | []]) -> [H | [N]];
insert(N, [H | T]) when N =< H -> [N | [H | T]];
insert(N, [H | T]) when N > H -> [H | insert(N, T)].

%
% 5. The function binary takes an integer N as input (assume that N ≥ 0). If N is equal to zero, it returns an empty list. 
% If N is greater than zero, it returns a list with a sequence of ones and zeros equivalent to the binary representation of N.
%

binary(0) -> [];
binary(N) -> binary(N div 2) ++ [N rem 2].

%
% 7. The function prime_factors takes an integer N as input (assume that N > 0), and returns a list 
% containing the prime factors of N in ascending order. 
% The prime factors are the prime numbers that divide a number exactly.
%

prime_factors(N) -> prime_factors(N, 2).

prime_factors(1, _) -> [];
prime_factors(N, I) when N rem I == 0 ->  [I | prime_factors(N div I, I)];
prime_factors(N, I) when N rem I /= 0 ->  prime_factors(N, I+1).


%
% 9. The function encode takes a list Lst as its argument. Consecutive duplicates of elements in Lst are 
% encoded as tuples {N, E} where N is the number of duplicates of the element E. If an element has no duplicates, 
% it is simply copied into the result list. 
%

encode([H | T]) -> encode(H, T, 1).

encode(First, [], 1) -> [First |  []];
encode(First, [], Count) -> [list_to_tuple([ Count | [First] ])] ++  [];
encode(First, [H | T], 1) when First /= H -> [First] ++ encode(H, T, 1);
encode(First, [H | T], Count) when First /= H -> [list_to_tuple([ Count | [First] ])] ++ encode(H, T, 1);
encode(First, [H | T], Count) when First == H -> encode(H, T, Count+1).
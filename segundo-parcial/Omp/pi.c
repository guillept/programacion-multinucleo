#include <stdio.h>
#include <time.h>
#include <omp.h>

#define NANOSECS_PER_SEC 1000000000

long getTime() {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  return t.tv_sec * NANOSECS_PER_SEC + t.tv_nsec;
}

int main(void) {
  long NUM_RECT = 10000000;
  double width = 1.0 / (double) NUM_RECT,
    area,
    sum = 0.0;

  long start = getTime();
  #pragma omp parallel for reduction(+:sum)
  for(long i = 0; i < NUM_RECT; i++)
  {
    double mid = (i + 0.5) * width;
    double height = 4.0 / (1.0 + mid * mid);
    sum += height;
  }
  area = width * sum;
  
  long end = getTime();
  printf("Pi = %.15f\n", area);
  printf("Time = %.2f\n", (double) (end - start) / NANOSECS_PER_SEC);
  return 0;
}
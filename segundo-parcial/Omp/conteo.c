#include <stdio.h>
#include <time.h>
#include <omp.h>
#include <stdlib.h>

#define NANOSECS_PER_SEC 1000000000
#define NUMEROS 100000

long getTime()
{
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return t.tv_sec * NANOSECS_PER_SEC + t.tv_nsec;
}

int *paralelCount(int *b)
{

    static int temp[NUMEROS];
    long count = 0;
#pragma omp parallel for reduction(+ \
                                   : count)
    for (long i = 0; i < NUMEROS; i++)
    {
        count = 0;
        for (long j = 0; j < NUMEROS; j++)
        {
            if (b[j] < b[i])
            {
                count++;
            }
            else if (b[i] == b[j] && j < i)
            {
                count++;
            }
        }
        temp[count] = b[i];
    }
    return temp;
}

int *secuentialCount(int *a)
{

    static int temp[NUMEROS];
    long count = 0;
    for (long i = 0; i < NUMEROS; i++)
    {
        count = 0;
        for (long j = 0; j < NUMEROS; j++)
        {
            if (a[j] < a[i])
            {
                count++;
            }
            else if (a[i] == a[j] && j < i)
            {
                count++;
            }
        }
        temp[count] = a[i];
    }

    return temp;
}

int main(void)
{
    int a[NUMEROS], b[NUMEROS];
    unsigned int next = 1, upper = 100, lower = 1;
    for (long i = 0; i < NUMEROS; i++)
    {
        int rnd = (rand() % (upper - lower + 1)) + lower;
        a[i] = rnd;
        b[i] = rnd;
    }

    // Secuencial
    long startSecuencial = getTime();
    int *res_a = secuentialCount(a);
    long endSecuencial = getTime();
    long timeSecuencial = endSecuencial - startSecuencial;
    printf("Time = %.2f\n", (double)(timeSecuencial) / NANOSECS_PER_SEC);

    // Paralelo
    long startParelelo = getTime();
    int *res_b = paralelCount(b);
    long endParalelo = getTime();
    long timeParalelo = endParalelo - startParelelo;
    printf("Time = %.2f\n", (double)(timeParalelo) / NANOSECS_PER_SEC);

    int iguales = 1;
    for (int i = 0; i < NUMEROS; i++)
        if (res_a[i] != res_b[i])
        {
            iguales = 0;
            break;
        };

    printf("Los arreglos %s son iguales\n", (iguales) ? "si" : "no");

    float speedup = (float)timeSecuencial / (float)timeParalelo;
    printf("Speedup: %f\n\n", speedup);
    return 0;
}